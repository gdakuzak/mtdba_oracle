CREATE OR REPLACE TRIGGER mktdba.engd_tg 
BEFORE INSERT ON mktdba.england_teams 
FOR EACH ROW

BEGIN
  SELECT engd_seq.NEXTVAL
  INTO   :new.id_team := engd_seq.nextval;
  FROM dual;
  
END;
/