select owner, object_name, status, object_type from dba_objects where object_type='TABLE' and owner like '%MTKDBA%'; q
select file_name, tablespace_name, status from dba_data_files:

create table mktdba.england_teams (
id_team varchar2(10),
name_team varchar2(200),
competition varchar2(100)
)
tablespace MTDBA_TBSPC
storage (initial 100K);

insert into mktdba.england_teams (id_team, name_team, competition)
values ('1','Bath','Premiership');
commit;


================= ver depois =================

ALTER TABLE mktdba.england_teams ADD (
  CONSTRAINT engd_pk primary key (id_team));
  
CREATE SEQUENCE mtkdba.engd_seq
	start with 1
	increment by 1;

CREATE OR REPLACE TRIGGER mktdba.engd_tg 
BEFORE INSERT ON mktdba.england_teams 
FOR EACH ROW

BEGIN
  SELECT engd_seq.NEXTVAL
  INTO   :new.id_team := engd_seq.nextval;
  FROM dual;
  
END;
/

insert into mktdba.england_teams (id_team, name_team, competition)
values (id_team, 'Bath', 'Premiership');
commit;
