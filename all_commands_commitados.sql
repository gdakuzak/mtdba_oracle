ALTER TABLE mktdba.england_teams ADD (
  CONSTRAINT engd_pk primary key (id_team));
  
commit;
  
CREATE SEQUENCE mtkdba.engd_seq
	start with 1
	increment by 1;

commit;
	
CREATE OR REPLACE TRIGGER mktdba.engd_tg 
BEFORE INSERT ON mktdba.england_teams 
FOR EACH ROW

BEGIN
  SELECT engd_seq.NEXTVAL
  INTO   :new.id_team := engd_seq.nextval;
  FROM dual;
  
END
/	

commit;