spool drop_all.txt
drop table mktdba.england_teams;
drop sequence mktdba.engd_seq;
drop trigger if exists mktdba.engd_tg;
commit;
spool off;