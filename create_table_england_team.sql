create table mktdba.england_teams (
id_team int,
name_team varchar2(200),
competition varchar2(100)
)
tablespace MTDBA_TBSPC
storage (initial 100K);

commit;